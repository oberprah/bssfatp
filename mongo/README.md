# mongoDB

The mongoDB-Server runs in a container and gets started with `docker-compose`. To persistent the data the db isn't stored in the container, but locally at `mongo/data/db`.

The keep a clean repo the db isn't checked in into git.

import {makeStyles, ThemeProvider} from "@material-ui/core";
import LogIn from "app/component/page/authentication/LogIn";
import SignUp from "app/component/page/authentication/SignUp";
import UserList from "app/component/page/authentication/userList/UserList";
import StartPage from "app/component/page/startPage/StartPage";
import {UserRouter} from "app/component/page/user/UserRouter";
import {getPathLogIn, getPathSignUp, getPathStartPage, getPathUser, getPathUserList} from "app/config/routeConfig";
import getTheme from "app/config/themeProvider";
import {AppState, store} from "app/store/store";
import React from 'react';
import {Provider, useSelector} from "react-redux";
import {BrowserRouter, Route} from 'react-router-dom';


export default function App() {
    return (
        <Provider store={store}>
            <AppWithStore/>
        </Provider>
    );
}


const useStyles = makeStyles({
    app: {
        minHeight: '100vh',
    },
});

function AppWithStore() {
    const classes = useStyles();

    const themeType = useSelector((state: AppState) => state.appSettings.themeType);
    const theme = getTheme(themeType);

    return (
        <ThemeProvider theme={theme}>
            <BrowserRouter>
                <div className={classes.app} style={{backgroundColor: theme.palette.background.default}}>
                    <Route exact path={getPathStartPage()} component={StartPage}/>
                    <Route exact path={getPathLogIn()} component={LogIn}/>
                    <Route exact path={getPathSignUp()} component={SignUp}/>
                    <Route exact path={getPathUserList()} component={UserList}/>
                    <Route path={getPathUser()} component={UserRouter}/>
                </div>
            </BrowserRouter>
        </ThemeProvider>
    )
}

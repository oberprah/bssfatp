import LogIn from "app/component/page/authentication/LogIn";
import {Home} from "app/component/page/user/home/Home";
import {JobRouter} from "app/component/page/user/jobDescription/JobRouter";
import {ProfilePage} from "app/component/page/user/profile/ProfilePage";
import {ProfilePageEdit} from "app/component/page/user/profile/ProfilePageEdit";
import {PublicProfilePage} from "app/component/page/user/profile/PublicProfilePage";
import {NavigationBar} from "app/component/util/appBar/NavigationBar";
import Content from "app/component/util/layout/Content";
import {
    getPathHome,
    getPathJob,
    getPathProfile,
    getPathProfileEdit,
    getPathPublicProfile
} from "app/config/routeConfig";
import {getUser} from "app/store/redux/user/userSelector";
import React from "react";
import {useSelector} from "react-redux";
import {Route} from "react-router";

export const UserRouter: React.FC<{}> = () => {
    const user = useSelector(getUser);

    if (user == null)
        return <LogIn/>;

    return (
        <>
            <NavigationBar/>
            <Content>
                <Route exact path={getPathHome()} render={() => <Home user={user}/>}/>
                <Route exact path={getPathProfile()} render={() => <ProfilePage user={user} editAllowed={true}/>}/>
                <Route exact path={getPathProfileEdit()} render={() => <ProfilePageEdit user={user}/>}/>
                <Route exact path={getPathPublicProfile(":username")}
                       render={(props) => <PublicProfilePage {...props}/>}/>
                <Route path={getPathJob(":jobId")} render={(props) => <JobRouter {...props} user={user}/>}/>
            </Content>
        </>
    );
};


import {apiCallApplication} from "app/api/application/apiCallApplication";
import {Application} from "app/api/application/application";
import {Chat} from "app/component/page/user/jobDescription/application/chat/Chat";
import {errorToast} from "app/component/util/toast";
import {getPathChat} from "app/config/routeConfig";
import {Job} from "app/store/redux/job/jobSlices";
import {User} from "app/store/redux/user/userSlice";
import React, {useEffect, useState} from "react";
import {match, Route} from "react-router";


export const ApplicationRouter: React.FC<{ match: match<{ applicationId: string }>, user: User, job: Job }> = ({match, user, job}) => {
    const [application, setApplication] = useState<Application | undefined>();

    useEffect(() => {
        apiCallApplication.getApplication(match.params.applicationId)
            .then(response => setApplication(response))
            .catch(_ => errorToast("Error on getApplication"))
    }, [job.id, match.params.applicationId, user.username]);

    if (application === undefined) {
        return <></>
    }

    return (
        <div>
            <Route exact path={getPathChat(":jobId", ":applicationId")}
                   render={() => <Chat
                       user={user}
                       application={application}
                       chatUsername={user.type === "BUSINESS" ? application.username : job.username}/>}/>
        </div>
    );
};

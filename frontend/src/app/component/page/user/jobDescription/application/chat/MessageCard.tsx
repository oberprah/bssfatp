import {Card, Grid, makeStyles, Typography} from "@material-ui/core";
import {Message} from "app/api/message/message";
import React from "react";


const useStyles = makeStyles({
    card: {
        padding: 8,
    },
});

export const MessageCard: React.FC<{ message: Message, owner: boolean }> = ({message, owner}) => {
    const classes = useStyles();

    return (
        <Grid container justify={owner ? "flex-end" : "flex-start"}>
            <Grid item xs={8}>
                <Card variant="outlined" className={classes.card}>
                    <Typography color="textPrimary">
                        {message.message}
                    </Typography>
                </Card>
            </Grid>
        </Grid>
    )
};

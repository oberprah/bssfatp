import {Card, Grid, InputAdornment, makeStyles, TextField} from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";

import SendRoundedIcon from '@material-ui/icons/SendRounded';
import {Application} from "app/api/application/application";
import {apiCallMessage} from "app/api/message/apiCallMessage";
import {getNewMessageId, Message} from "app/api/message/message";
import {MessageCard} from "app/component/page/user/jobDescription/application/chat/MessageCard";
import {errorToast} from "app/component/util/toast";
import {UserInfo} from "app/component/util/userInfo/UserInfo";
import {User} from "app/store/redux/user/userSlice";
import React, {useEffect, useState} from "react";

const useStyles = makeStyles({
    messageField: {
        width: "100%",
    },
    card: {
        padding: 16,
    },
});

export const Chat: React.FC<{ user: User, application: Application, chatUsername: string }> = ({user, application, chatUsername}) => {
    const classes = useStyles();

    const [messages, setMessages] = useState<Message[]>([]);
    const [currentMessageText, setCurrentMessageText] = useState<string>("");

    useEffect(() => {
        fetchMessages();
        const interval = setInterval(() => fetchMessages(), 5000);
        return () => clearInterval(interval);

        function fetchMessages() {
            apiCallMessage.getAllMessages(application.id)
                .then(messages => setMessages(messages))
        }
    }, [application.id]);

    function sendMessage() {
        if (currentMessageText) {
            const currentMessage: Message = {
                id: getNewMessageId(),
                applicationId: application.id,
                username: user.username,
                date: new Date(),
                message: currentMessageText
            };
            apiCallMessage.postMessage(currentMessage)
                .then(responseMessage => {
                    setMessages(messages.concat(responseMessage));
                    setCurrentMessageText("");
                })
                .catch(_ => errorToast("Couldn't save the message in the backend"))
        }
    }

    return (
        <Grid container spacing={2}>
            <Grid item xs={12}>
                <Card className={classes.card}>
                    <UserInfo username={chatUsername}/>
                </Card>
            </Grid>
            {messages.map(message =>
                <Grid item xs={12} key={message.id}>
                    <MessageCard message={message} owner={user.username === message.username}/>
                </Grid>)}
            <Grid item xs={12} container spacing={2} alignItems="center">
                <Grid item xs>
                    <TextField
                        value={currentMessageText}
                        placeholder="Message"
                        variant="outlined"
                        onChange={(event) => setCurrentMessageText(event.target.value)}
                        onKeyPress={(event) => {
                            if (event.key === 'Enter')
                                sendMessage()
                        }}
                        className={classes.messageField}
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton onClick={() => sendMessage()} disabled={!currentMessageText}>
                                        <SendRoundedIcon color={!currentMessageText ? "disabled" : "secondary"}/>
                                    </IconButton>
                                </InputAdornment>
                            ),
                        }}
                    />
                </Grid>
            </Grid>
        </Grid>
    );
};

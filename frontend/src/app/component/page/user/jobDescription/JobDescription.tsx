import {Grid, IconButton, makeStyles, Theme, Typography} from "@material-ui/core";
import DeleteRoundedIcon from '@material-ui/icons/DeleteRounded';
import EditRoundedIcon from '@material-ui/icons/EditRounded';
import {apiCallJob} from "app/api/job/apiCallJob";
import {ApplicationList} from "app/component/page/user/jobDescription/component/ApplicationList";
import {ApplyButton} from "app/component/page/user/jobDescription/component/ApplyButton";
import {CenteredImage} from "app/component/page/user/jobDescription/component/CenteredImage";
import {JobDescriptionInfoBox} from "app/component/page/user/jobDescription/component/JobDescriptionInfoBox";
import {JobDescriptionTextBox} from "app/component/page/user/jobDescription/component/JobDescriptionTextBox";
import {errorToast} from "app/component/util/toast";
import {getPathHome, getPathJobEdit, getPathProfile, getPathPublicProfile} from "app/config/routeConfig";
import {deleteJob, Job} from "app/store/redux/job/jobSlices";
import {User} from "app/store/redux/user/userSlice";
import React from "react";
import {useDispatch} from "react-redux";
import {useHistory} from "react-router";


const useStyles = makeStyles((theme: Theme) => ({
    iconPrimaryColor: {
        color: theme.palette.text.primary,
    },
    pointer: {
        cursor: "pointer",
    },
}));

export const JobDescription: React.FC<{ user: User, job: Job }> = ({user, job}) => {
    const dispatch = useDispatch();
    const history = useHistory();
    const classes = useStyles();

    function navigateToEditMode() {
        history.push(getPathJobEdit(job.id));
    }

    function navigateToProfile() {
        if (user.username === job.username) {
            history.push(getPathProfile());
        } else {
            history.push(getPathPublicProfile(job.username));
        }
    }

    function deleteJobFinally(job: Job) {
        apiCallJob.deleteJob(job)
            .then(_ => {
                dispatch(deleteJob(job));
                history.push(getPathHome());
            })
            .catch(err => {
                errorToast("Couldn't delete job");
                console.log(err);
            })
    }

    return (
        <Grid container spacing={2}>
            {
                job.image
                    ? <Grid item xs={12}>
                        <CenteredImage image={job.image}/>
                    </Grid>
                    : <></>
            }
            <Grid item xs={10}>
                <Typography variant="h2" color="textPrimary">
                    {`${job.title}`}
                </Typography>
            </Grid>
            <Grid container item xs={2} justify="flex-end" alignContent="center">
                {getIcons(job)}
            </Grid>
            <Grid item xs={12}>
                <Typography color="textPrimary" className={classes.pointer} onClick={() => navigateToProfile()}>
                    <b>@{job.username}</b>
                </Typography>
            </Grid>
            {job.location || job.expectedEarnings || job.jobTypes.length ?
                <Grid item xs={12} md={4} container spacing={2}>
                    <Grid item xs={12}>
                        <JobDescriptionInfoBox job={job}/>
                    </Grid>
                </Grid>
                : <></>}
            {job.description || job.tags.length ?
                <Grid item xs={12} md={8} container spacing={2}>
                    <Grid item xs={12}>
                        <JobDescriptionTextBox job={job}/>
                    </Grid>
                </Grid>
                : <></>}
            {user.type === "BUSINESS" ? <Grid item xs={12}><ApplicationList job={job}/></Grid> : <></>}
        </Grid>
    );

    function getIcons(job: Job) {
        if (user.type === "INFLUENCER")
            return getIconsInfluencer();

        if (user.type === "BUSINESS")
            return getIconsBusiness(job);

        return <></>
    }

    function getIconsInfluencer() {
        return <ApplyButton user={user} job={job}/>
    }

    function getIconsBusiness(job: Job) {
        if (job.username !== user.username) {
            return <></>
        }

        return (<>
            <IconButton onClick={() => navigateToEditMode()}>
                <EditRoundedIcon className={classes.iconPrimaryColor}/>
            </IconButton>
            <IconButton onClick={() => deleteJobFinally(job)}>
                <DeleteRoundedIcon className={classes.iconPrimaryColor}/>
            </IconButton>
        </>)
    }
};

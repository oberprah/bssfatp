import {Typography} from "@material-ui/core";
import {ApplicationRouter} from "app/component/page/user/jobDescription/application/ApplicationRouter";
import {JobDescription} from "app/component/page/user/jobDescription/JobDescription";
import {JobDescriptionEdit} from "app/component/page/user/jobDescription/JobDescriptionEdit";
import {getPathChat, getPathJob, getPathJobEdit} from "app/config/routeConfig";
import {getJobById} from "app/store/redux/job/jobSelector";
import {Job} from "app/store/redux/job/jobSlices";
import {User} from "app/store/redux/user/userSlice";
import React from "react";
import {useSelector} from "react-redux";
import {match, Route} from "react-router";


export const ID_NEW_JOB = 'new';

export const JobRouter: React.FC<{ match: match<{ jobId: string }>, user: User }> = ({match, user}) => {
    let job = useSelector(getJobById(match.params.jobId));

    if (match.params.jobId === "new") {
        job = getNewJob(user.username);
    }

    if (job === undefined) {
        return <Typography variant="h2" color="textPrimary">
            Job Not Found!
        </Typography>
    }

    return (
        <div>
            <Route exact path={getPathJob(":jobId")}
                   render={() => <JobDescription user={user} job={job as Job}/>}/>
            <Route exact path={getPathJobEdit(":jobId")}
                   render={(props) => <JobDescriptionEdit {...props} job={job as Job}/>}/>
            <Route path={getPathChat(":jobId", ":applicationId")}
                   render={(props) => <ApplicationRouter {...props} user={user} job={job as Job}/>}/>
        </div>
    );
};

function getNewJob(username: string): Job {
    return {
        id: ID_NEW_JOB,
        username: username,
        title: "",
        description: "",
        tags: [],
        image: "",
        jobTypes: [],
        expectedEarnings: undefined,
        location: "",
    };
}

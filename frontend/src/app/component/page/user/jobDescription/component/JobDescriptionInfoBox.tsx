import {Card, Grid, IconButton, makeStyles, Theme, Typography} from "@material-ui/core";
import LaunchRoundedIcon from '@material-ui/icons/LaunchRounded';
import {Job} from "app/store/redux/job/jobSlices";
import React from "react";


const useStyles = makeStyles((theme: Theme) => ({
    iconPrimaryColor: {
        color: theme.palette.text.primary,
    },
    card: {
        padding: 8,
    },
}));

export const JobDescriptionInfoBox: React.FC<{ job: Job }> = ({job}) => {
    const classes = useStyles();

    function openMapsInNewTab(location: string) {
        const url = `http://maps.google.com/maps/place/${location}`;
        const win = window.open(url, '_blank');
        if (win)
            win.focus();
    }

    return <Card variant="outlined" className={classes.card}>
        <Grid container spacing={2}>
            {job.jobTypes.length ?
                <Grid item xs={12}>
                    <Typography color="textPrimary">
                        <b>Type</b><br/> {job.jobTypes.map(t => `${t}`).join(", ")}
                    </Typography>
                </Grid>
                : <></>}
            {job.expectedEarnings ?
                <Grid item xs={12}>
                    <Typography color="textPrimary">
                        <b>Expected Earnings</b><br/> {job.expectedEarnings} Euro
                    </Typography>
                </Grid>
                : <></>}
            {job.location ?
                <Grid item xs={12} container spacing={1}>
                    <Grid item>
                        <Typography color="textPrimary">
                            <b>Location</b><br/> {job.location}
                        </Typography>
                    </Grid>
                    <Grid item>
                        <IconButton onClick={() => openMapsInNewTab(job.location)}>
                            <LaunchRoundedIcon className={classes.iconPrimaryColor} fontSize="small"/>
                        </IconButton>
                    </Grid>
                </Grid>
                : <></>}
        </Grid>
    </Card>
};

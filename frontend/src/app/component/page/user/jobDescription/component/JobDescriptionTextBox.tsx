import {Card, Grid, makeStyles, Theme, Typography} from "@material-ui/core";
import {Job} from "app/store/redux/job/jobSlices";
import React from "react";


const useStyles = makeStyles((theme: Theme) => ({
    iconPrimaryColor: {
        color: theme.palette.text.primary,
    },
    card: {
        padding: 8,
    },
}));

export const JobDescriptionTextBox: React.FC<{ job: Job }> = ({job}) => {
    const classes = useStyles();

    return <Card variant="outlined" className={classes.card}>
        <Grid container spacing={2}>
            {job.description ?
                <Grid item xs={12}>
                    <Typography color="textPrimary">
                        {job.description}
                    </Typography>
                </Grid>
                : <></>}
            {job.tags.length ?
                <Grid item xs={12}>
                    <Typography color="textPrimary">
                        <b>{job.tags.map(t => `#${t}`).join(" ")}</b>
                    </Typography>
                </Grid>
                : <></>}
        </Grid>
    </Card>
};

import {Typography} from '@material-ui/core';
import ListSubheader from '@material-ui/core/ListSubheader';
import {makeStyles, useTheme} from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Autocomplete, {RenderGroupParams} from '@material-ui/lab/Autocomplete';
import {citiesGermany} from "app/component/page/user/jobDescription/component/locationSearch/citiesGermany";
import React from 'react';
import {ListChildComponentProps, VariableSizeList} from 'react-window';

const LISTBOX_PADDING = 8; // px

function renderRow(props: ListChildComponentProps) {
    const {data, index, style} = props;
    return React.cloneElement(data[index], {
        style: {
            ...style,
            top: (style.top as number) + LISTBOX_PADDING,
        },
    });
}

const OuterElementContext = React.createContext({});

const OuterElementType = React.forwardRef<HTMLDivElement>((props, ref) => {
    const outerProps = React.useContext(OuterElementContext);
    return <div ref={ref} {...props} {...outerProps} />;
});

// Adapter for react-window
const ListboxComponent = React.forwardRef<HTMLDivElement>(function ListboxComponent(props, ref) {
    const {children, ...other} = props;
    const itemData = React.Children.toArray(children);
    const theme = useTheme();
    const smUp = useMediaQuery(theme.breakpoints.up('sm'), {noSsr: true});
    const itemCount = itemData.length;
    const itemSize = smUp ? 36 : 48;

    const getChildSize = (child: React.ReactNode) => {
        if (React.isValidElement(child) && child.type === ListSubheader) {
            return 48;
        }

        return itemSize;
    };

    const getHeight = () => {
        if (itemCount > 8) {
            return 8 * itemSize;
        }
        return itemData.map(getChildSize).reduce((a, b) => a + b, 0);
    };

    return (
        <div ref={ref}>
            <OuterElementContext.Provider value={other}>
                <VariableSizeList
                    itemData={itemData}
                    height={getHeight() + 2 * LISTBOX_PADDING}
                    width="100%"
                    key={itemCount}
                    outerElementType={OuterElementType}
                    innerElementType="ul"
                    itemSize={(index: any) => getChildSize(itemData[index])}
                    overscanCount={5}
                    itemCount={itemCount}
                >
                    {renderRow}
                </VariableSizeList>
            </OuterElementContext.Provider>
        </div>
    );
});

const useStyles = makeStyles({
    listbox: {
        '& ul': {
            padding: 0,
            margin: 0,
        },
    },
});

const renderGroup = (params: RenderGroupParams) => [
    <ListSubheader key={params.key} component="div">
        <b><i>{params.key}</i></b>
    </ListSubheader>,
    params.children,
];

export const LocationSearch: React.FC<{
    location: string,
    setLocation: (location: string) => void
}> = ({location, setLocation}) => {
    const classes = useStyles();

    return (
        <Autocomplete
            id="virtualize-demo"
            value={location}
            onChange={(event: any, newValue: string | null) => setLocation(newValue || "")}
            style={{width: "100%"}}
            disableListWrap
            classes={classes}
            ListboxComponent={ListboxComponent as React.ComponentType<React.HTMLAttributes<HTMLElement>>}
            renderGroup={renderGroup}
            options={citiesGermany}
            groupBy={location => location[0].toUpperCase()}
            renderInput={params => (
                <TextField
                    {...params}
                    variant="outlined"
                    label="Location"
                    fullWidth/>
            )}
            renderOption={option => <Typography noWrap>{option}</Typography>}
        />
    );
};

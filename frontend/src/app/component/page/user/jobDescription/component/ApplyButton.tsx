import {Button} from "@material-ui/core";
import {apiCallApplication} from "app/api/application/apiCallApplication";
import {Application, getNewApplicationId} from "app/api/application/application";
import {errorToast} from "app/component/util/toast";
import {getPathChat} from "app/config/routeConfig";
import {Job} from "app/store/redux/job/jobSlices";
import {User} from "app/store/redux/user/userSlice";
import React from "react";
import {useHistory} from "react-router";

export const ApplyButton: React.FC<{ user: User, job: Job }> = ({user, job}) => {
    const history = useHistory();

    function apply() {
        apiCallApplication.getAllApplications(job.id, user.username)
            .then(applications => {
                if (applications.length === 0) {
                    const newApplication: Application = {
                        id: getNewApplicationId(),
                        jobId: job.id,
                        username: user.username,
                    };
                    apiCallApplication.postApplication(newApplication)
                        .then(application => history.push(getPathChat(job.id, application.id)))
                        .catch(_ => errorToast("Couldn't post application"))
                }
                if (applications.length === 1) {
                    history.push(getPathChat(job.id, applications[0].id))
                }
                if (applications.length > 1) {
                    console.log(applications);
                    errorToast("More than one applications returned")
                }
            })
            .catch(_ => errorToast("Couldn't fetch applications"));
    }

    return (
        <Button variant="contained" color="primary" onClick={() => apply()}>
            Chat
        </Button>
    )
};

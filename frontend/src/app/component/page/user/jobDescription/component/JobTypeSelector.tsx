import {Checkbox, FormControlLabel, Grid, Typography} from "@material-ui/core";
import {JobType} from "app/store/redux/job/jobSlices";
import React from "react";


export const JobTypeSelector: React.FC<{
    jobTypes: Set<JobType>,
    setJobTypes: (jobTypes: Set<JobType>) => void,
}> = ({jobTypes, setJobTypes}) => {

    function toggleJobType(jobType: JobType) {
        jobTypes.has(jobType)
            ? jobTypes.delete(jobType)
            : jobTypes.add(jobType);
        setJobTypes(new Set<JobType>(jobTypes));
    }

    function createCombobox(jobType: JobType) {
        return <Grid item>
            <FormControlLabel
                control={<Checkbox checked={jobTypes.has(jobType)} onChange={() => toggleJobType(jobType)}/>}
                label={<Typography color="textPrimary">{jobType}</Typography>}/>
        </Grid>
    }

    return <Grid container spacing={2}>
        {createCombobox("Youtube Video")}
        {createCombobox("Facebook Post")}
        {createCombobox("Instagram Post")}
        {createCombobox("Instagram Story")}
        {createCombobox("Other")}
    </Grid>
};

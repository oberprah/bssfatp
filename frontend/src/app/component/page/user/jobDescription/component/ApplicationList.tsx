import {Button, Card, Grid, makeStyles, Typography} from "@material-ui/core";
import {apiCallApplication} from "app/api/application/apiCallApplication";
import {Application} from "app/api/application/application";
import {getPathChat} from "app/config/routeConfig";
import {Job} from "app/store/redux/job/jobSlices";
import React, {useEffect, useState} from "react";
import {useHistory} from "react-router";

const useStyles = makeStyles({
    card: {
        padding: 8,
    },
});

export const ApplicationList: React.FC<{ job: Job }> = ({job}) => {
    const classes = useStyles();
    const history = useHistory();
    const [applications, setApplications] = useState<Application[]>([]);

    useEffect(() => {
        apiCallApplication.getAllApplications(job.id)
            .then(response => setApplications(response))
    }, [job.id]);

    function chat(job: Job, application: Application) {
        history.push(getPathChat(job.id, application.id))
    }

    return (
        <Grid container spacing={2}>
            {
                applications.map(application =>
                    <Grid item xs={12} key={application.id}>
                        <Card className={classes.card} variant="outlined">
                            <Grid container alignItems="center">
                                <Grid item xs>
                                    <Typography noWrap>
                                        {application.username}
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    <Button variant="contained" color="primary" onClick={() => chat(job, application)}>
                                        Chat
                                    </Button>
                                </Grid>
                            </Grid>
                        </Card>
                    </Grid>
                )
            }
        </Grid>
    )
};

import {Avatar, makeStyles} from "@material-ui/core";
import React from "react";


const useStyles = makeStyles({
    image: {
        height: "100%",
        maxHeight: 375,
        width: "100%",
    },
});

export const CenteredImage: React.FC<{ image: string }> = ({image}) => {
    const classes = useStyles();

    return <Avatar src={image} className={classes.image} variant="square"/>
};

import {Button, Grid, InputAdornment, makeStyles, TextField} from "@material-ui/core";
import EuroIcon from '@material-ui/icons/Euro';
import {apiCallJob} from "app/api/job/apiCallJob";
import {CenteredImage} from "app/component/page/user/jobDescription/component/CenteredImage";
import {JobTypeSelector} from "app/component/page/user/jobDescription/component/JobTypeSelector";
import {LocationSearch} from "app/component/page/user/jobDescription/component/locationSearch/LocationSearch";
import {ID_NEW_JOB} from "app/component/page/user/jobDescription/JobRouter";
import {ImagePicker} from "app/component/page/user/profile/components/ImagePicker";
import {TagSelector} from "app/component/util/tag/TagSeletor";
import {errorToast} from "app/component/util/toast";
import {getPathHome, getPathJob} from "app/config/routeConfig";
import {addJob, Job, JobType, updateJob} from "app/store/redux/job/jobSlices";
import {getNewJobId, jsonToJob} from "app/store/redux/job/jobUtil";
import React, {useState} from "react";
import {useDispatch} from "react-redux";
import {useHistory} from "react-router";

const useStyles = makeStyles({
    maxWidth: {
        width: "100%",
    },
});

export const JobDescriptionEdit: React.FC<{ job: Job }> = ({job}) => {
    const dispatch = useDispatch();
    const history = useHistory();
    const classes = useStyles();

    const [title, setTitle] = useState(job !== undefined ? job.title : "");
    const [description, setDescription] = useState(job !== undefined ? job.description : "");
    const [tagList, setTagList] = useState<string[]>(job.tags);
    const [image, setImage] = useState<string>(job.image);
    const [jobTypes, setJobTypes] = useState<Set<JobType>>(new Set(job.jobTypes));
    const [expectedEarning, setExpectedEarning] = useState<number | undefined>(job.expectedEarnings);
    const [location, setLocation] = useState<string>(job.location);

    function cancel() {
        history.push(
            job.id === ID_NEW_JOB
                ? getPathHome()
                : getPathJob(job.id)
        )
    }

    function save() {
        const jobToSave: Job = {
            id: job.id === ID_NEW_JOB ? getNewJobId() : job.id,
            username: job.username,
            title: title,
            description: description,
            tags: tagList,
            image: image,
            jobTypes: Array.from(jobTypes),
            expectedEarnings: expectedEarning,
            location: location,
        };
        job.id === ID_NEW_JOB ? saveAddJob(jobToSave) : saveUpdateJob(jobToSave);
    }

    function saveAddJob(job: Job) {
        apiCallJob.postJob(job)
            .then(returnedJob => {
                dispatch(addJob(jsonToJob(returnedJob)));
                history.push(getPathJob(job.id));
            })
            .catch(_ => errorToast("Couldn't save the user in the backend"));
    }

    function saveUpdateJob(job: Job) {
        apiCallJob.putJob(job)
            .then(returnedJob => {
                dispatch(updateJob(returnedJob));
                history.push(getPathJob(job.id));
            })
            .catch(_ => errorToast("Couldn't save the user in the backend"));
    }

    return (
        <Grid container spacing={2}>
            {image
                ? <Grid item xs={12}>
                    <CenteredImage image={image}/>
                </Grid>
                : <></>
            }
            <Grid item xs={12}>
                <ImagePicker setImageAsBase64={(img) => setImage(img)} maxWidth={1024} maxHeight={1024}/>
            </Grid>
            <Grid item xs={12}>
                <TextField
                    className={classes.maxWidth}
                    variant="outlined"
                    label="Title"
                    value={title}
                    onChange={event => setTitle(event.target.value)}/>
            </Grid>
            <Grid item xs={12}>
                <TextField
                    className={classes.maxWidth}
                    variant="outlined"
                    label="Description"
                    multiline
                    rows={12}
                    value={description}
                    onChange={event => setDescription(event.target.value)}/>
            </Grid>
            <Grid item xs>
                <LocationSearch location={location} setLocation={setLocation}/>
            </Grid>
            <Grid item>
                <TextField
                    variant="outlined"
                    label="Expected Earnings"
                    value={expectedEarning || ""}
                    onChange={event => {
                        const re = /^[0-9\b]+$/;
                        if (event.target.value === '' || re.test(event.target.value)) {
                            setExpectedEarning(Number.parseInt(event.target.value));
                        }
                    }}
                    InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                                <EuroIcon fontSize="small"/>
                            </InputAdornment>
                        ),
                    }}/>
            </Grid>
            <Grid item xs={12}>
                <JobTypeSelector jobTypes={jobTypes} setJobTypes={setJobTypes}/>
            </Grid>
            <Grid item xs={12}>
                <TagSelector tagList={tagList} setTagList={setTagList}/>
            </Grid>
            <Grid item xs={12} container justify="flex-end" spacing={2}>
                <Grid item>
                    <Button variant="outlined" onClick={() => cancel()}>
                        Cancel
                    </Button>
                </Grid>
                <Grid item>
                    <Button variant="contained" color="primary" onClick={() => save()}>
                        Save
                    </Button>
                </Grid>
            </Grid>
        </Grid>
    )
};

import {Card, Grid, makeStyles, Theme, Typography} from "@material-ui/core";
import {getPathJob} from "app/config/routeConfig";
import {Job} from "app/store/redux/job/jobSlices";
import React from "react";
import {useHistory} from "react-router"

const useStyles = makeStyles((theme: Theme) => ({
    card: {
        padding: theme.spacing(2),
        cursor: "pointer",
    },
    title: {
        paddingBottom: 8,
    },
}));

export const JobListItem: React.FC<{ job: Job }> = ({job}) => {
    const history = useHistory();
    const classes = useStyles();

    function openJobDescription() {
        history.push(getPathJob(job.id));
    }

    return (
        <Card className={classes.card} onClick={() => openJobDescription()} variant="outlined">
            <Grid container>
                <Grid item xs={12}>
                    <Typography variant="h2" color="textPrimary" noWrap className={classes.title}>
                        {job.title}
                    </Typography>
                </Grid>
                <Grid item xs={12}>
                    {job.expectedEarnings || job.location ?
                        <Typography color="textPrimary" noWrap>
                            {job.expectedEarnings ? `${job.expectedEarnings} Euro - ` : ""}<i>{job.location}</i>
                        </Typography>
                        : <></>}
                    {job.tags.length ?
                        <Typography color="textPrimary" noWrap>
                            <b>{job.tags.map(t => `#${t}`).join(" ")}</b>
                        </Typography>
                        : <></>}
                </Grid>
            </Grid>
        </Card>
    )
};

import {Grid, IconButton, Typography} from "@material-ui/core";
import Popover from '@material-ui/core/Popover';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import TuneRoundedIcon from '@material-ui/icons/TuneRounded';
import {TextFieldEuro} from "app/component/page/user/home/component/selectJobFilter/TextFieldEuro";
import {LocationSearch} from "app/component/page/user/jobDescription/component/locationSearch/LocationSearch";
import {TagSelector} from "app/component/util/tag/TagSeletor";
import React, {useState} from 'react';

export interface JobFilter {
    location?: string;
    expectedEarnings?: {
        from?: number,
        to?: number,
    };
    tagList?: string[];
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        content: {
            padding: theme.spacing(2),
        },
    }),
);

export const SelectJobFilter: React.FC<{
    jobFilter: JobFilter,
    setJobFilter: (jobFilter: JobFilter) => void
}> = ({jobFilter, setJobFilter}) => {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);

    const handleClick = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const open = Boolean(anchorEl);
    const id = open ? 'simple-popover' : undefined;

    return (
        <div>
            <IconButton onClick={handleClick}>
                <TuneRoundedIcon/>
            </IconButton>
            <Popover
                id={id}
                open={open}
                anchorEl={anchorEl}
                onClose={handleClose}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}
            >
                <Grid container spacing={2} className={classes.content}>
                    <Grid item xs={12}>
                        <Typography>
                            Filter
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <LocationSearch
                            location={jobFilter.location || ""}
                            setLocation={(location) => setJobFilter({...jobFilter, location: location})}/>
                    </Grid>
                    <Grid item xs={12} container spacing={1}>
                        <Grid item xs={6}>
                            <TextFieldEuro
                                label="From"
                                value={jobFilter.expectedEarnings?.from || ""}
                                setValue={(value: number) =>
                                    setJobFilter({
                                        ...jobFilter,
                                        expectedEarnings: {
                                            ...jobFilter.expectedEarnings,
                                            from: value
                                        }
                                    })}/>
                        </Grid>
                        <Grid item xs={6}>
                            <TextFieldEuro
                                label="To"
                                value={jobFilter.expectedEarnings?.to || ""}
                                setValue={(value: number) =>
                                    setJobFilter({
                                        ...jobFilter,
                                        expectedEarnings: {
                                            ...jobFilter.expectedEarnings,
                                            to: value
                                        }
                                    })}/>
                        </Grid>
                    </Grid>
                    <Grid item xs={12}>
                        <TagSelector
                            tagList={jobFilter.tagList || []}
                            setTagList={(tagList: string[]) =>
                                setJobFilter({
                                    ...jobFilter,
                                    tagList: tagList,
                                })}/>
                    </Grid>
                </Grid>
            </Popover>
        </div>
    );
};

import {BaseTextFieldProps, InputAdornment, TextField} from "@material-ui/core";
import EuroSymbolRoundedIcon from '@material-ui/icons/EuroSymbolRounded';
import React from "react";

export const TextFieldEuro: React.FC<{
    label: string,
    value: BaseTextFieldProps["value"],
    setValue: (value: number) => void,
}> = ({label, value, setValue}) => {
    return <TextField
        fullWidth
        variant="outlined"
        label={label}
        value={value}
        onChange={event => {
            const re = /^[0-9\b]+$/;
            if (event.target.value === '' || re.test(event.target.value)) {
                setValue(Number.parseInt(event.target.value));
            }
        }}
        InputProps={{
            startAdornment: (
                <InputAdornment position="start">
                    <EuroSymbolRoundedIcon fontSize="small"/>
                </InputAdornment>
            ),
        }}/>
};

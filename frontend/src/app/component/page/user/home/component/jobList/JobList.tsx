import {Grid, makeStyles, Theme} from "@material-ui/core";
import {useJobsForUser} from "app/api/job/useJob";
import {JobListItem} from "app/component/page/user/home/component/jobList/JobListItem";
import {JobFilter} from "app/component/page/user/home/component/selectJobFilter/SelectJobFilter";
import {Job} from "app/store/redux/job/jobSlices";
import {User} from "app/store/redux/user/userSlice";
import React from "react";


const useStyles = makeStyles((theme: Theme) => ({
    container: {
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(3),
    }
}));

export const JobList: React.FC<{
    user: User,
    jobFilter?: JobFilter,
}> = ({user, jobFilter}) => {
    const classes = useStyles();
    const jobs = useJobsForUser(user);

    function applyFilter(job: Job) {
        if (jobFilter) {
            if (jobFilter.location && jobFilter.location !== job.location)
                return false;
            if (jobFilter.expectedEarnings?.from && (!job.expectedEarnings || jobFilter.expectedEarnings.from > job.expectedEarnings))
                return false;
            if (jobFilter.expectedEarnings?.to && (!job.expectedEarnings || jobFilter.expectedEarnings.to < job.expectedEarnings))
                return false;
            if (jobFilter.tagList?.length && !jobFilter.tagList.filter(x => job.tags.includes(x)).length)
                return false;
        }
        return true;
    }

    return (
        <Grid container spacing={2} className={classes.container}>
            {jobs.filter(job => applyFilter(job))
                .map(job => {
                    return (<Grid item xs={12} key={job.id}>
                        <JobListItem job={job}/>
                    </Grid>)
                })}

        </Grid>
    )
};

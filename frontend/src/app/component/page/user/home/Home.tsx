import {Button, Grid} from "@material-ui/core";
import {JobList} from "app/component/page/user/home/component/jobList/JobList";
import {JobFilter, SelectJobFilter} from "app/component/page/user/home/component/selectJobFilter/SelectJobFilter";
import {ID_NEW_JOB} from "app/component/page/user/jobDescription/JobRouter";
import {getPathJobEdit} from "app/config/routeConfig";
import {User} from "app/store/redux/user/userSlice";
import React, {useState} from "react";
import {useHistory} from "react-router";

export const Home: React.FC<{ user: User }> = ({user}) => {
    const history = useHistory();
    const [jobFilter, setJobFilter] = useState<JobFilter>({});

    function navigateToNewJob() {
        history.push(getPathJobEdit(ID_NEW_JOB))
    }

    return (
        <>
            <Grid container spacing={2} justify="flex-end" alignItems="center">
                <Grid item>
                    <SelectJobFilter jobFilter={jobFilter} setJobFilter={setJobFilter}/>
                </Grid>
                {
                    user.type === "BUSINESS"
                        ? <Grid item>
                            <Button variant="contained" color="primary" onClick={() => navigateToNewJob()}>
                                Add Job
                            </Button>
                        </Grid>
                        : <></>
                }
            </Grid>
            <JobList user={user} jobFilter={jobFilter}/>
        </>)
};

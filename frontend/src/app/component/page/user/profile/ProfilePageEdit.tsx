import {Avatar, Button, Grid, makeStyles, TextField} from "@material-ui/core";
import {apiCallUser} from "app/api/user/apiCallUser";
import {ImagePicker} from "app/component/page/user/profile/components/ImagePicker";
import {TagSelector} from "app/component/util/tag/TagSeletor";
import {errorToast} from "app/component/util/toast";
import {getPathProfile} from "app/config/routeConfig";
import {setUser, User} from "app/store/redux/user/userSlice";
import React, {useState} from "react";
import {useDispatch} from "react-redux";
import {useHistory} from "react-router";


const useStyles = makeStyles({
    maxWidth: {
        width: "100%",
    },
    avatar: {
        width: 300,
        height: 300,
    },
});

export const ProfilePageEdit: React.FC<{ user: User }> = ({user}) => {
    const dispatch = useDispatch();
    const history = useHistory();

    const classes = useStyles();

    const [lastName, setLastName] = useState(user.lastName);
    const [firstName, setFirstName] = useState(user.firstName);
    const [email, setEmail] = useState(user.email);
    const [avatarAsBase64, setAvatarAsBase64] = useState(user.avatar);
    const [description, setDescription] = useState(user.description);
    const [tagList, setTagList] = useState<string[]>(user.tags);

    function exitEditMode() {
        history.push(getPathProfile());
    }

    function cancel() {
        exitEditMode()
    }

    function save() {
        const userToSave: User = {
            ...user,
            email: email,
            firstName: firstName,
            lastName: lastName,
            avatar: avatarAsBase64,
            description: description,
            tags: tagList,
        };
        apiCallUser.putUser(userToSave)
            .then(returnedUser => {
                dispatch(setUser(returnedUser));
                exitEditMode();
            })
            .catch(_ => errorToast("Couldn't save the user in the backend"));
    }

    return (
        <Grid container spacing={2}>
            <Grid item xs={12} container justify="center">
                <Avatar
                    alt="Remy Sharp"
                    src={avatarAsBase64}
                    className={classes.avatar}/>
            </Grid>
            <Grid item xs={12}>
                <ImagePicker setImageAsBase64={(avatar) => setAvatarAsBase64(avatar)}/>
            </Grid>
            <Grid item xs={6}>
                <TextField
                    label="First Name"
                    variant="outlined"
                    defaultValue={firstName}
                    onChange={event => setFirstName(event.target.value)}
                    className={classes.maxWidth}
                />
            </Grid>
            <Grid item xs={6}>
                <TextField
                    label="Last Name"
                    variant="outlined"
                    defaultValue={lastName}
                    onChange={event => setLastName(event.target.value)}
                    className={classes.maxWidth}
                />
            </Grid>
            <Grid item xs={12}>
                <TextField
                    label="Email"
                    variant="outlined"
                    defaultValue={email}
                    onChange={event => setEmail(event.target.value)}
                    className={classes.maxWidth}
                />
            </Grid>
            <Grid item xs={12}>
                <TextField
                    label="About You"
                    variant="outlined"
                    defaultValue={description}
                    multiline rows="4"
                    onChange={event => setDescription(event.target.value)}
                    className={classes.maxWidth}
                />
            </Grid>
            <Grid item>
                <TagSelector tagList={tagList} setTagList={setTagList}/>
            </Grid>
            <Grid item xs={12} container justify="flex-end" spacing={2}>
                <Grid item>
                    <Button variant="outlined" onClick={() => cancel()}>
                        Cancel
                    </Button>
                </Grid>
                <Grid item>
                    <Button variant="contained" color="primary" onClick={() => save()}>
                        Save
                    </Button>
                </Grid>
            </Grid>
        </Grid>
    );
};

import {apiCallUser} from "app/api/user/apiCallUser";
import {ProfilePage} from "app/component/page/user/profile/ProfilePage";
import {User} from "app/store/redux/user/userSlice";
import React, {useEffect, useState} from "react";
import {match} from "react-router";

export const PublicProfilePage: React.FC<{ match: match<{ username: string }> }> = ({match}) => {
    const [user, setUser] = useState<User | undefined>();

    useEffect(() => {
        apiCallUser.getUser(match.params.username)
            .then(response => setUser(response));
    }, [match.params.username]);

    if (!user)
        return <></>;

    return (
        <ProfilePage user={user}/>
    )
};

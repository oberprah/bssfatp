import {Avatar, Grid, IconButton, makeStyles, Theme, Typography} from "@material-ui/core";
import EditIcon from '@material-ui/icons/Edit';
import FacebookIcon from '@material-ui/icons/Facebook';
import InstagramIcon from '@material-ui/icons/Instagram';
import TwitterIcon from '@material-ui/icons/Twitter';
import YouTubeIcon from '@material-ui/icons/YouTube';
import {getPathProfileEdit} from "app/config/routeConfig";
import {User} from "app/store/redux/user/userSlice";
import React from "react";
import {useHistory} from "react-router";

const useStyles = makeStyles((theme: Theme) => ({
    colorIcon: {
        color: theme.palette.text.primary,
    },
    avatar: {
        width: 300,
        height: 300,
    },
}));

export const ProfilePage: React.FC<{ user: User, editAllowed?: boolean }> = ({user, editAllowed = false}) => {
    const history = useHistory();
    const classes = useStyles();

    function navigateToEditMode() {
        history.push(getPathProfileEdit());
    }

    function openInNewTab(url: string) {
        alert("Opens just a dummy page to show how it should work");
        const win = window.open(url, '_blank');
        if (win)
            win.focus();
    }

    return (
        <Grid container spacing={2}>
            <Grid item xs={1}/>
            <Grid item xs={10} container justify="center">
                <Avatar
                    alt="Remy Sharp"
                    src={user.avatar}
                    className={classes.avatar}/>
            </Grid>
            <Grid container item xs={1} justify="flex-end">
                <Grid item>
                    {editAllowed
                        ? <IconButton onClick={() => navigateToEditMode()}>
                            <EditIcon className={classes.colorIcon}/>
                        </IconButton>
                        : <></>
                    }
                </Grid>
            </Grid>
            <Grid item xs={12}>
                <Typography variant="h2" color="textPrimary">
                    {`${user.firstName} ${user.lastName}`}
                </Typography>
            </Grid>
            <Grid item xs={12}>
                <Typography color="textPrimary">
                    {user.email}
                </Typography>
            </Grid>
            <Grid item xs={12} container spacing={2}>
                <Grid item>
                    <IconButton onClick={() => openInNewTab("https://www.facebook.com/LeoMessi")}>
                        <FacebookIcon className={classes.colorIcon}/>
                    </IconButton>
                </Grid>
                <Grid item>
                    <IconButton onClick={() => openInNewTab("https://www.instagram.com/cristiano/")}>
                        <InstagramIcon className={classes.colorIcon}/>
                    </IconButton>
                </Grid>
                <Grid item>
                    <IconButton onClick={() => openInNewTab("https://twitter.com/realdonaldtrump")}>
                        <TwitterIcon className={classes.colorIcon}/>
                    </IconButton>
                </Grid>
                <Grid item>
                    <IconButton onClick={() => openInNewTab("https://www.youtube.com/user/PewDiePie/featured")}>
                        <YouTubeIcon className={classes.colorIcon}/>
                    </IconButton>
                </Grid>
            </Grid>
            <Grid item xs={12}>
                <Typography color="textPrimary">
                    {user.description}
                </Typography>
            </Grid>
            <Grid item xs={12}>
                <Typography color="textPrimary">
                    <b>{user.tags.map(t => `#${t}`).join(" ")}</b>
                </Typography>
            </Grid>
        </Grid>
    );
};

import {Grid, makeStyles, Paper, Typography} from "@material-ui/core";
import RootRef from '@material-ui/core/RootRef'
import {convertImageToBase64} from "app/util/imageUtil";
import React from "react";
import {useDropzone} from "react-dropzone";

const useStyles = makeStyles({
    paper: {
        padding: 8,
        cursor: "pointer",
    },
});

export const ImagePicker: React.FC<{
    setImageAsBase64: (image: string) => void,
    maxWidth?: number,
    maxHeight?: number
}> = ({setImageAsBase64, maxWidth = 720, maxHeight = 720}) => {
    const {getRootProps, getInputProps} = useDropzone({onDrop});
    const {ref, ...rootProps} = getRootProps();

    const classes = useStyles();

    function onDrop(files: any) {
        convertImageToBase64(files[0], (result) => setImageAsBase64(result), maxWidth, maxHeight);
    }

    return (
        <Grid container justify="center">
            <Grid item>
                <RootRef rootRef={ref}>
                    <Paper {...rootProps} className={classes.paper} variant="outlined">
                        <input {...getInputProps()} />
                        <Typography>
                            Drag 'n' drop an image here, or click to select one
                        </Typography>
                    </Paper>
                </RootRef>
            </Grid>
        </Grid>
    )
};

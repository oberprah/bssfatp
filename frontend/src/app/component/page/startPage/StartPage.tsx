import {Button, makeStyles, Typography} from "@material-ui/core";
import Center from "app/component/util/layout/Center";
import {getPathLogIn, getPathSignUp} from "app/config/routeConfig";
import React from "react";
import {useHistory} from "react-router";


const useStyles = makeStyles({
    home: {
        textAlign: 'center',
    },
    header: {
        minHeight: '100vh',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttons: {
        display: 'grid',
        gridTemplateColumns: 'repeat(2, 1fr);',
        gridColumnGap: '12px',
    }
});

export default function StartPage() {
    const classes = useStyles();

    const history = useHistory();

    function navigateTo(path: string) {
        history.push(path)
    }

    return (
        <Center>
            <Typography variant="h1" color="textPrimary" gutterBottom>
                HyFluenz
            </Typography>
            <div className={classes.buttons}>
                <Button variant="contained" color="primary" size="large" onClick={() => navigateTo(getPathLogIn())}>
                    Log In
                </Button>
                <Button variant="text" color="default" size="large" onClick={() => navigateTo(getPathSignUp())}>
                    Sign Up
                </Button>
            </div>
        </Center>
    );
}


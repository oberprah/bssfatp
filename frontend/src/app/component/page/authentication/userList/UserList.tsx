import {Container, Grid, makeStyles, Typography} from "@material-ui/core";
import {apiCallUser} from "app/api/user/apiCallUser";
import {UserListItem} from "app/component/page/authentication/userList/UserListItem";
import {User} from "app/store/redux/user/userSlice";
import React, {useEffect, useState} from "react";


const useStyles = makeStyles({
    userList: {
        paddingTop: 24,
        paddingBottom: 24,
    },
});

export default function UserList() {
    const classes = useStyles();
    const [userList, setUserList] = useState<User[] | undefined>();

    useEffect(() => {
        apiCallUser.getAllUser()
            .then(users => setUserList(users));
    }, []);

    if (userList === undefined)
        return <></>;

    const userInfluencer = userList.filter(user => user.type === "INFLUENCER");
    const userBusiness = userList.filter(user => user.type === "BUSINESS");

    return (
        <Container maxWidth="md" className={classes.userList}>
            <Grid container spacing={2}>
                {createList(userInfluencer, "Influencer")}
                {createList(userBusiness, "Business Owner")}
            </Grid>
        </Container>
    );

    function createList(userList: User[], title: string) {
        return (<>
            <Grid item xs={12}>
                <Typography variant="h5" color="textPrimary">
                    {title}
                </Typography>
            </Grid>
            {userList.map(user =>
                <Grid item xs={12} key={user.username}>
                    <UserListItem user={user}/>
                </Grid>)}
        </>)
    }
}

import {Button, Card, Grid, makeStyles, Typography} from "@material-ui/core";
import {getPathHome} from "app/config/routeConfig";
import {setUser, User} from "app/store/redux/user/userSlice";
import React from "react";
import {useDispatch} from "react-redux";
import {useHistory} from "react-router";

const useStyles = makeStyles({
    card: {
        padding: 24,
    }
});

export const UserListItem: React.FC<{ user: User }> = ({user}) => {
    const dispatch = useDispatch();
    const history = useHistory();
    const classes = useStyles();

    function logIn(user: User) {
        dispatch(setUser(user));
        history.push(getPathHome());
    }

    return (
        <Card className={classes.card} variant="outlined">
            <Grid container>
                <Grid item xs>
                    <Typography variant="h4">
                        {user.username}
                    </Typography>
                    <Typography variant="body1">
                        {user.email}
                    </Typography>
                </Grid>
                <Grid item xs={1} container justify="center" alignItems="center">
                    <Button
                        variant="outlined"
                        onClick={() => logIn(user)}
                    >
                        login
                    </Button>
                </Grid>
            </Grid>
        </Card>
    )
};

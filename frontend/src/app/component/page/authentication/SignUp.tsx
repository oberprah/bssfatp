import {Button, Card, Grid, makeStyles, TextField, Theme, Typography} from "@material-ui/core";
import Center from "app/component/util/layout/Center";
import React from "react";


const useStyles = makeStyles((theme: Theme) => ({
    card: {
        minHeight: '400px',
        minWidth: '400px',
        backgroundColor: theme.palette.background.paper,
        padding: '30px',
    },
    inputField: {
        width: 250,
    },
}));

export default function SignUp() {
    const classes = useStyles();

    function signUp() {
        alert("Not implemented yet");
    }

    return (
        <Center>
            <Card className={classes.card} variant="outlined">
                <Grid container direction="column" justify="center" alignItems="center" spacing={3}>
                    <Grid item>
                        <Typography variant="h3" color="textPrimary" gutterBottom>
                            Sign Up
                        </Typography>
                    </Grid>
                    <Grid item>
                        <TextField
                            label="Username"
                            variant="outlined"
                            className={classes.inputField}/>
                    </Grid>
                    <Grid item>
                        <TextField
                            label="Password"
                            variant="outlined"
                            type="password"
                            className={classes.inputField}/>
                    </Grid>
                    <Grid item>
                        <TextField
                            label="Repeat Password"
                            variant="outlined"
                            type="password"
                            className={classes.inputField}/>
                    </Grid>
                    <Grid item>
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={() => signUp()}
                        >
                            Sign Up
                        </Button>
                    </Grid>
                </Grid>
            </Card>
        </Center>
    );
}

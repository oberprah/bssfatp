import {Button, Card, Grid, makeStyles, TextField, Theme, Typography} from "@material-ui/core";
import Center from "app/component/util/layout/Center";
import {getPathUserList} from "app/config/routeConfig";
import React from "react";
import {useHistory} from "react-router";

const useStyles = makeStyles((theme: Theme) => ({
    card: {
        height: 400,
        width: 400,
        backgroundColor: theme.palette.background.paper,
        padding: 30,
        marginBottom: 20,
    },
    container: {
        height: 400,
    },
    inputField: {
        width: 250,
    },
    skip: {
        cursor: 'pointer'
    },
}));

export default function LogIn() {
    const classes = useStyles();
    const history = useHistory();

    function navigateTo(path: string) {
        history.push(path)
    }

    function logIn() {
        alert("Not Implemented yet");
    }

    return (
        <Center>
            <Card className={classes.card} variant="outlined">
                <Grid container direction="column" justify="center" alignItems="center" spacing={3}
                      className={classes.container}>
                    <Grid item>
                        <Typography variant="h3" color="textPrimary" gutterBottom>
                            Login
                        </Typography>
                    </Grid>
                    <Grid item>
                        <TextField label="Username" variant="outlined" className={classes.inputField}/>
                    </Grid>
                    <Grid item>
                        <TextField label="Password" variant="outlined" type="password" className={classes.inputField}/>
                    </Grid>
                    <Grid item>
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={() => logIn()}
                        >
                            Login
                        </Button>
                    </Grid>
                </Grid>
            </Card>
            <Typography
                className={classes.skip}
                variant="body1" color="textPrimary"
                onClick={() => navigateTo(getPathUserList())}
            >
                Skip
            </Typography>
        </Center>
    );
}

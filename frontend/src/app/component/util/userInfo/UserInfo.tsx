import {Avatar, Grid, makeStyles, Typography} from "@material-ui/core";
import {apiCallUser} from "app/api/user/apiCallUser";
import {getPathPublicProfile} from "app/config/routeConfig";
import {User} from "app/store/redux/user/userSlice";
import React, {useEffect, useState} from "react";
import {useHistory} from "react-router";


const useStyles = makeStyles({
    container: {
        cursor: "pointer",
    },
});

export const UserInfo: React.FC<{ username: string }> = ({username}) => {
    const history = useHistory();
    const classes = useStyles();
    const [user, setUser] = useState<User | undefined>();

    useEffect(() => {
        apiCallUser.getUser(username)
            .then(response => setUser(response));
    }, [username]);

    return (
        <Grid
            container
            spacing={2}
            alignItems="center"
            onClick={() => history.push(getPathPublicProfile(username))}
            className={classes.container}
        >
            <Grid item>
                <Avatar src={user ? user.avatar : ""}/>
            </Grid>
            <Grid item>
                <Typography color="textPrimary" variant="h4">
                    {user ? `${user.firstName} ${user.lastName}` : `@${username}`}
                </Typography>
            </Grid>
        </Grid>
    )
};

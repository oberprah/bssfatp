import {Card, Grid, InputAdornment, makeStyles, TextField, Typography} from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import AddCircleOutlineRoundedIcon from '@material-ui/icons/AddCircleOutlineRounded';
import ClearIcon from '@material-ui/icons/Clear';
import React, {useState} from "react";


const useStyles = makeStyles({
    spaceStart: {
        paddingLeft: 16,
    },
});

export const TagSelector: React.FC<{ tagList: string[], setTagList: (tagList: string[]) => void }> = ({tagList, setTagList}) => {
    const classes = useStyles();

    const [tag, setTag] = useState("");

    function addTag() {
        const hashTag = tag.toLowerCase().replace(" ", "");
        if (hashTag && !tagList.includes(hashTag)) {
            setTagList(tagList.concat(hashTag));
            setTag("");
        }
    }

    function removeTag(tag: string) {
        setTagList(tagList.filter(t => t !== tag))
    }

    return (
        <Grid container spacing={2} alignItems="center">
            {tagList.map(item =>
                <Grid item key={item}>
                    <Card variant="outlined">
                        <Grid container alignItems="center">
                            <Grid item>
                                <Typography color="textPrimary" className={classes.spaceStart}>
                                    #{item}
                                </Typography>
                            </Grid>
                            <Grid item>
                                <IconButton onClick={() => removeTag(item)}>
                                    <ClearIcon fontSize="small"/>
                                </IconButton>
                            </Grid>
                        </Grid>
                    </Card>
                </Grid>)
            }
            <Grid item>
                <TextField
                    label="Tag"
                    variant="outlined"
                    value={tag}
                    onChange={event => setTag(event.target.value)}
                    onKeyPress={(event) => {
                        if (event.key === 'Enter')
                            addTag()
                    }}
                    InputProps={{
                        endAdornment: (
                            <InputAdornment position="end">
                                <IconButton onClick={() => addTag()}>
                                    <AddCircleOutlineRoundedIcon color="secondary"/>
                                </IconButton>
                            </InputAdornment>
                        ),
                    }}/>
            </Grid>
        </Grid>
    )
};

import {
    AppBar,
    Avatar,
    Button,
    Container,
    Grid,
    IconButton,
    makeStyles,
    Theme,
    Toolbar,
    Typography
} from "@material-ui/core";
import Brightness4RoundedIcon from '@material-ui/icons/Brightness4Rounded';
import Brightness7RoundedIcon from '@material-ui/icons/Brightness7Rounded';
import {getPathHome, getPathProfile, getPathStartPage} from "app/config/routeConfig";
import {setAppSettings} from "app/store/redux/appSettings/appSettingsSlice";
import {getUser} from "app/store/redux/user/userSelector";
import {resetUser} from "app/store/redux/user/userSlice";
import {AppState} from "app/store/store";
import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {useHistory} from "react-router";

const useStyles = makeStyles((theme: Theme) => ({
    appBar: {
        color: theme.palette.text.primary,
        backgroundColor: theme.palette.background.default,
    },
    toolbar: {
        padding: 0,
    },
    title: {
        cursor: "pointer",
    },
    avatar: {
        cursor: "pointer",
    },
}));

export const NavigationBar: React.FC<{}> = () => {
    const dispatch = useDispatch();
    const classes = useStyles();
    const history = useHistory();
    const user = useSelector(getUser);
    const themeType = useSelector((state: AppState) => state.appSettings.themeType);

    function logOut() {
        dispatch(resetUser());
        history.push(getPathStartPage());
    }

    return (
        <>
            <AppBar className={classes.appBar}>
                <Container maxWidth="lg">
                    <Toolbar className={classes.toolbar}>
                        <Grid container alignItems="center">
                            <Grid item xs container alignContent="flex-start" alignItems="center">
                                <Button variant="outlined" style={{textTransform: "capitalize"}}>
                                    <Typography
                                        color="textPrimary"
                                        variant="h4"
                                        onClick={() => history.push(getPathHome())}
                                        className={classes.title}
                                    >
                                        HyFluenz
                                    </Typography>
                                </Button>
                            </Grid>
                            <Grid item xs={8} container spacing={2} justify="flex-end" alignItems="center">

                                {user != null
                                    ? <>
                                        <Grid item>
                                            <Button color="inherit" onClick={() => logOut()} size="large">
                                                Log Out
                                            </Button>
                                        </Grid>
                                        <Grid item>
                                            <Avatar
                                                src={user.avatar}
                                                onClick={() => history.push(getPathProfile())}
                                                className={classes.avatar}
                                            />
                                        </Grid>
                                    </>
                                    : <></>}
                                <Grid item>
                                    <IconButton onClick={() => {
                                        dispatch(setAppSettings({themeType: themeType === "light" ? "dark" : "light"}))
                                    }}>
                                        {themeType === "light" ? <Brightness4RoundedIcon/> : <Brightness7RoundedIcon/>}
                                    </IconButton>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Toolbar>
                </Container>
            </AppBar>
            <Toolbar/>
        </>
    )
};

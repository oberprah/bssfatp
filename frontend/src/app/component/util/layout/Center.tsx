import {makeStyles} from "@material-ui/core";
import React from "react";


const useStyles = makeStyles({
    center: {
        minHeight: '100vh',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
    },
});

export default function Center(
    props: {
        children: JSX.Element[] | JSX.Element
    }
) {
    const classes = useStyles();

    return (
        <header className={classes.center}>
            {props.children}
        </header>
    );
}

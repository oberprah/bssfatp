import {Container, makeStyles} from "@material-ui/core";
import React from "react";


const useStyles = makeStyles({
    container: {
        paddingTop: 24,
        paddingBottom: 24,
    },
});

export default function Content(
    props: {
        children: React.ReactNode,
    }
) {
    const classes = useStyles();

    return (
        <Container maxWidth="lg" className={classes.container}>
            {props.children}
        </Container>
    )
}

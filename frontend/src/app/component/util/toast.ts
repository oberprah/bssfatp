import cogoToast, {Options} from "cogo-toast";

const options: Options = {
    position: "bottom-left"
};

export const successToast = (msg: string) => cogoToast.success(msg, options);
export const errorToast = (msg: string) => cogoToast.error(msg, options);
export const infoToast = (msg: string) => cogoToast.info(msg, options);
export const loadingToast = (msg: string) => cogoToast.loading(msg, options);
export const warnToast = (msg: string) => cogoToast.warn(msg, options);

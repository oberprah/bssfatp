import {createSlice, PayloadAction} from "@reduxjs/toolkit";


export interface AppSettings {
    themeType: "light" | "dark";
}

export type AppSettingsState = AppSettings;

const appSettingsSlice = createSlice({
    name: "user",
    initialState: {themeType: "light"} as AppSettingsState,
    reducers: {
        setAppSettings: (state: AppSettings, action: PayloadAction<AppSettings>) => action.payload,
    }
});

export const appSettingsReducer = appSettingsSlice.reducer;

export const {
    setAppSettings
} = appSettingsSlice.actions;

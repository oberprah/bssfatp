import {User} from "app/store/redux/user/userSlice";
import {AppState} from "app/store/store";

export const getAllJobs = (state: AppState) => state.jobs;

export const getJobsForUser = (user: User) => {
    if (user.type === "INFLUENCER")
        return getAllJobs;
    if (user.type === "BUSINESS")
        return (state: AppState) => state.jobs.filter(job => job.username === user.username);

    throw Error(`getJobsForUser for user.type '${user.type}' not implemented`)
};

export const getJobById = (jobId: string) => {
    return (state: AppState) => state.jobs.find(job => job.id === jobId)
};



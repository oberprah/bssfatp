import {Job} from "app/store/redux/job/jobSlices";

export function getNewJobId(): string {
    return `${Math.ceil(Math.random() * 1000000)}`
}

export function jsonToJob(json: any): Job {
    return {
        id: json.id,
        username: json.username,
        title: json.title || "",
        description: json.description || "",
        tags: json.tags || [],
        image: json.image || "",
        jobTypes: json.jobTypes || [],
        expectedEarnings: json.expectedEarnings,
        location: json.location || ""
    }
}

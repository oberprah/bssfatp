import {createSlice, PayloadAction} from "@reduxjs/toolkit";


export type JobType =
    "Youtube Video"
    | "Facebook Post"
    | "Instagram Post"
    | "Instagram Story"
    | "Other";

export interface Job {
    id: string;
    username: string;
    title: string;
    description: string;
    tags: string[];
    image: string;
    jobTypes: JobType[];
    expectedEarnings: number | undefined;
    location: string;
}

export type JobState = Job[];

const userSlice = createSlice({
    name: "user",
    initialState: [] as JobState,
    reducers: {
        setJobs: (state: JobState, action: PayloadAction<Job[]>) => action.payload,
        addJob: (state: JobState, action: PayloadAction<Job>) => state.concat([action.payload]),
        updateJob: (state: JobState, action: PayloadAction<Job>) => {
            const oldJob = state.find(job => job.id === action.payload.id) as Job;
            const index = state.indexOf(oldJob);
            state[index] = action.payload
        },
        deleteJob: (state: JobState, action: PayloadAction<Job>) => state.filter(job => job.id !== action.payload.id)
    }
});

export const jobReducer = userSlice.reducer;

export const {
    setJobs,
    addJob,
    updateJob,
    deleteJob,
} = userSlice.actions;


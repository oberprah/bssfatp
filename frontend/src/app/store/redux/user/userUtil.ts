import {User} from "app/store/redux/user/userSlice";

export function jsonToUser(json: any): User {
    return {
        username: json.username,
        email: json.email ? json.email : "",
        type: json.type,
        firstName: json.firstName ? json.firstName : "",
        lastName: json.lastName ? json.lastName : "",
        avatar: json.avatar ? json.avatar : "",
        description: json.description ? json.description : "",
        tags: json.tags ? json.tags : [],
    }
}

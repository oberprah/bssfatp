import {AppState} from "app/store/store";

export const getUser = (state: AppState) => state.user;

import {createSlice, PayloadAction} from "@reduxjs/toolkit";

export interface User {
    username: string;
    email: string;
    type: "INFLUENCER" | "BUSINESS"
    firstName: string;
    lastName: string;
    avatar: string;
    description: string;
    tags: string[];
}

export type UserState = User | null;

const userSlice = createSlice({
    name: "user",
    initialState: null as UserState,
    reducers: {
        setUser: (state: UserState, action: PayloadAction<User>) => action.payload,
        resetUser: (state: UserState) => state = null
    }
});

export const userReducer = userSlice.reducer;

export const setUser = userSlice.actions.setUser;
export const resetUser = userSlice.actions.resetUser;

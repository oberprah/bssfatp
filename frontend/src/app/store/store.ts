import {applyMiddleware, combineReducers, createStore} from "@reduxjs/toolkit";
import {appSettingsReducer} from "app/store/redux/appSettings/appSettingsSlice";
import {jobReducer} from "app/store/redux/job/jobSlices";
import {userReducer} from "app/store/redux/user/userSlice";

import {composeWithDevTools} from "redux-devtools-extension";
import thunkMiddleware from "redux-thunk";

const rootReducer = combineReducers({
    appSettings: appSettingsReducer,
    user: userReducer,
    jobs: jobReducer,
});

export type AppState = ReturnType<typeof rootReducer>;

function configureStore() {
    const middleware = [thunkMiddleware];
    const middleWareEnhancer = applyMiddleware(...middleware);

    return createStore(
        rootReducer,
        composeWithDevTools(middleWareEnhancer)
    )
}

export const store = configureStore();

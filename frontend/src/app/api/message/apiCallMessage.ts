import {apiCall, createApiCallUrl} from "app/api/apiCall";
import {jsonToMessage, Message} from "app/api/message/message";
import {BACKEND_URL} from "app/config/backendCalls";

const URL = `${BACKEND_URL}/messages`;

export const apiCallMessage = {
    getAllMessages,
    postMessage,
};

function getAllMessages(applicationId?: string): Promise<Message[]> {
    const queryParameter = [
        applicationId ? `applicationId=${applicationId}` : undefined,
    ];
    return apiCall.get(createApiCallUrl(URL, queryParameter))
        .then(response => response.data.map((message: any) => jsonToMessage(message)))
}

function postMessage(message: Message): Promise<Message> {
    return apiCall.post(URL, message)
        .then(response => jsonToMessage(response.data));
}

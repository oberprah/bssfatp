export interface Message {
    id: string,
    applicationId: string,
    username: string,
    date: Date,
    message: string,
}

export function getNewMessageId(): string {
    return `${Math.ceil(Math.random() * 1000000)}`
}

export function jsonToMessage(json: any): Message {
    return {
        id: json.id,
        applicationId: json.applicationId,
        username: json.username,
        date: new Date(json.date),
        message: json.message,
    }
}

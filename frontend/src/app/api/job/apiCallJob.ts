import {apiCall} from "app/api/apiCall";
import {BACKEND_URL} from "app/config/backendCalls";
import {Job} from "app/store/redux/job/jobSlices";
import {jsonToJob} from "app/store/redux/job/jobUtil";


const URL = `${BACKEND_URL}/jobs`;

export const apiCallJob = {
    getAllJobs: getAllJobs,
    putJob: putJob,
    postJob: postJob,
    deleteJob: deleteJob,
};

function getAllJobs(): Promise<Job[]> {
    return apiCall.get(URL)
        .then(response => response.data.map((job: any) => jsonToJob(job)))
}

function putJob(job: Job): Promise<Job> {
    return apiCall.put(`${URL}/${job.id}`, job)
        .then(response => jsonToJob(response.data))
}

function postJob(job: Job): Promise<Job> {
    return apiCall.post(URL, job)
        .then(response => jsonToJob(response.data))
}

function deleteJob(job: Job): Promise<any> {
    return apiCall.delete(`${URL}/${job.id}`)
}

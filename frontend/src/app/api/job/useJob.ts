import {apiCallJob} from "app/api/job/apiCallJob";
import {getJobsForUser} from "app/store/redux/job/jobSelector";
import {JobState, setJobs} from "app/store/redux/job/jobSlices";
import {User} from "app/store/redux/user/userSlice";
import {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";


export const useJobsForUser = (user: User): JobState => {
    const dispatch = useDispatch();
    const jobs = useSelector(getJobsForUser(user));

    useEffect(() => {
        apiCallJob.getAllJobs()
            .then(jobs => dispatch(setJobs(jobs)))
    }, [dispatch]);

    return jobs;
};

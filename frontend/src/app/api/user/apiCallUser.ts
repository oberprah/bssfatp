import {apiCall} from "app/api/apiCall";
import {BACKEND_URL} from "app/config/backendCalls";
import {User} from "app/store/redux/user/userSlice";
import {jsonToUser} from "app/store/redux/user/userUtil";


const URL = `${BACKEND_URL}/users`;

export const apiCallUser = {
    getAllUser,
    getUser,
    putUser,
};

function getAllUser(): Promise<User[]> {
    return apiCall.get(URL)
        .then(response => response.data.map((user: any) => jsonToUser(user)))
}

function getUser(username: string): Promise<User> {
    return apiCall.get(`${URL}/${username}`)
        .then(response => jsonToUser(response.data))
}

function putUser(user: User): Promise<User> {
    return apiCall.put(`${URL}/${user.username}`, user)
        .then(response => jsonToUser(response.data))
}

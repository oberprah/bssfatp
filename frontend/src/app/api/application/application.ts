export interface Application {
    id: string,
    jobId: string,
    username: string,
}

export function jsonToApplication(json: any): Application {
    return {
        id: json.id,
        jobId: json.jobId,
        username: json.username,
    }
}

export function getNewApplicationId(): string {
    return `${Math.ceil(Math.random() * 1000000)}`
}

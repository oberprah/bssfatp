import {apiCall, createApiCallUrl} from "app/api/apiCall";
import {Application, jsonToApplication} from "app/api/application/application";
import {BACKEND_URL} from "app/config/backendCalls";

const URL = `${BACKEND_URL}/applications`;

export const apiCallApplication = {
    getAllApplications,
    getApplication,
    postApplication,
};

function getAllApplications(jobId?: string, username?: string): Promise<Application[]> {
    const queryParameter = [
        jobId ? `jobId=${jobId}` : undefined,
        username ? `username=${username}` : undefined,
    ];
    return apiCall.get(createApiCallUrl(URL, queryParameter))
        .then(response => response.data.map((json: any) => jsonToApplication(json)))
}

function getApplication(applicationId: string) {
    return apiCall.get(`${URL}/${applicationId}`)
        .then(response => jsonToApplication(response.data))
}

function postApplication(application: Application): Promise<Application> {
    return apiCall.post(URL, application)
        .then(response => jsonToApplication(response.data))
}

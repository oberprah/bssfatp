import axios from 'axios';

export const apiCall = {
    get: axios.get,
    post: axios.post,
    put: axios.put,
    delete: axios.delete,
};

export function createApiCallUrl(baseUrl: string, parameter: Array<string | undefined>): string {
    const urlQuery = parameter
        .filter(q => q !== undefined)
        .join('&');
    return urlQuery ? `${baseUrl}?${urlQuery}` : baseUrl;
}

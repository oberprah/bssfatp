import {createMuiTheme} from "@material-ui/core";
import {blue, orange} from "@material-ui/core/colors";
import {PaletteOptions} from "@material-ui/core/styles/createPalette";
import {AppSettings} from "app/store/redux/appSettings/appSettingsSlice";

export default function getTheme(theme: AppSettings["themeType"]) {
    return createMuiTheme({
        typography: {
            fontFamily: 'Quicksand',
            h1: {
                fontFamily: "Quicksand",
            },
            h4: {
                fontSize: 24,
            },
        },
        shape: {
            borderRadius: 14,
        },
        palette: theme === 'dark' ? paletteDark : paletteLight,
    });
}

const paletteLight: PaletteOptions = {
    primary: blue,
    secondary: orange,
    text: {
        primary: '#000000'
    },
    background: {
        default: '#ffffff',
        paper: '#fafcff'
    },
    type: 'light'
};

const paletteDark: PaletteOptions = {
    primary: blue,
    secondary: orange,
    text: {
        primary: '#ffffff'
    },
    background: {
        default: '#616161',
        paper: '#595959'
    },
    type: 'dark'
};

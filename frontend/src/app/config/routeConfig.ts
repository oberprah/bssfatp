export const getPathStartPage = () => "/";
export const getPathLogIn = () => "/login";
export const getPathUserList = () => "/login/user-list";
export const getPathSignUp = () => "/sign-up";

export const getPathUser = () => "/user";
export const getPathHome = () => `${getPathUser()}/home`;
export const getPathProfile = () => `${getPathUser()}/profile`;
export const getPathProfileEdit = () => `${getPathProfile()}/edit`;
export const getPathPublicProfile = (username: string) => `${getPathUser()}/public-profile/${username}`;
export const getPathJob = (jobId: string) => `${getPathUser()}/job/${jobId}`;
export const getPathJobEdit = (jobId: string) => `${getPathJob(jobId)}/edit`;
export const getPathChat = (jobId: string, applicationId: string) => `${getPathJob(jobId)}/application/${applicationId}`;

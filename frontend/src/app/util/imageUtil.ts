export function convertImageToBase64(file: any, cb: (result: string) => void, maxWidth = 720, maxHeight = 720) {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
        let image = new Image();
        image.src = reader.result as string;

        image.onload = function () {
            let imageWidth = image.width,
                imageHeight = image.height;

            if (imageWidth > imageHeight) {
                if (imageWidth > maxWidth) {
                    imageHeight *= maxWidth / imageWidth;
                    imageWidth = maxWidth;
                }
            } else {
                if (imageHeight > maxHeight) {
                    imageWidth *= maxHeight / imageHeight;
                    imageHeight = maxHeight;
                }
            }

            const canvas = document.createElement('canvas');
            canvas.width = imageWidth;
            canvas.height = imageHeight;

            const ctx = canvas.getContext("2d");
            if (ctx !== null)
                ctx.drawImage(image, 0, 0, imageWidth, imageHeight);

            // The resized file ready for upload
            cb(canvas.toDataURL(file.type));
        };
    };
    reader.onerror = function (error) {
        console.log('Error: ', error);
    };
}

# Setup

Build and tag the Docker image:

```zsh
$ docker build -t <name>:dev .
```

Then, spin up the container once the build is done:

```zsh
$ docker run -v ${PWD}:/app -v /app/node_modules -p 3001:3000 --rm <name>:dev
```

> If you run into an `"ENOENT: no such file or directory, open '/app/package.json".` error, you may need to add an additional volume: `-v /app/package.json`.

What’s happening here?

1. The docker run command creates a new container instance, from the image we just created, and runs it.

2. `-v ${PWD}:/app` mounts the code into the container at “/app”.
   > {PWD} may not work on Windows. See this Stack Overflow question for more info.

3. Since we want to use the container version of the “node_modules” folder, we configured another volume: `-v /app/node_modules`. You should now be able to remove the local “node_modules” flavor.

4. `-p 3001:3000` exposes port 3000 to other Docker containers on the same network (for inter-container communication) and port 3001 to the host.

5. Finally, `--rm` removes the container and volumes after the container exits.

Open your browser to http://localhost:3001/ and you should see the app. Try making a change to the App component within your code editor. You should see the app hot-reload. Kill the server once done.

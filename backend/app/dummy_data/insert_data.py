from dummy_data.dummy_jobs import get_job_beer_garden, get_job_vegan_bakery, get_job_burger_house
from dummy_data.dummy_users import USER_ALEX_HUBER, USER_JOHN_DOE, USER_MARRY_SMITH
from mongo.mongo import user_collection, job_collection


def insert_dummy_data_if_db_is_empty():
    if user_collection.count() == 0 and job_collection.count() == 0:
        _insert_dummy_data()


def _insert_dummy_data():
    user_collection.insert_one(USER_ALEX_HUBER.dict())
    job_collection.insert_one(get_job_beer_garden(USER_ALEX_HUBER).dict())

    user_collection.insert_one(USER_JOHN_DOE.dict())
    job_collection.insert_one(get_job_vegan_bakery(USER_JOHN_DOE).dict())
    job_collection.insert_one(get_job_burger_house(USER_JOHN_DOE).dict())

    user_collection.insert_one(USER_MARRY_SMITH.dict())

from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from api.jobs import jobs_api
from api.jobs.applications import applications_api
from api.jobs.applications.messages import messages_api
from api.users import users_api
from dummy_data.insert_data import insert_dummy_data_if_db_is_empty

app = FastAPI()

origins = [
    "http://localhost",
    "http://localhost:8080",
    "http://localhost:3000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(users_api.router)
app.include_router(jobs_api.router)
app.include_router(applications_api.router)
app.include_router(messages_api.router)

insert_dummy_data_if_db_is_empty()

from typing import List

from pydantic import BaseModel


class User(BaseModel):
    username: str
    email: str
    type: str
    firstName: str
    lastName: str
    avatar: str
    description: str
    tags: List[str]

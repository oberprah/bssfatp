from fastapi import APIRouter, HTTPException

from api.users.user import User
from mongo.mongo import user_collection

URL_USERS = "/users"
URL_USERS_USERNAME = URL_USERS + "/{username}"

router = APIRouter()


@router.get(URL_USERS)
def get_users():
    user_list = user_collection.find({}, {'_id': False})
    return list(user_list)


@router.get(URL_USERS_USERNAME)
def get_user(username: str):
    return user_collection.find_one({"username": username}, {'_id': False})


@router.post(URL_USERS)
def insert_user(user: User):
    user_collection.insert_one(user.dict())
    return user


@router.put(URL_USERS_USERNAME)
def update_user(username: str, user: User):
    result = user_collection.update_one({"username": username}, {"$set": user.dict()})

    if result.matched_count != 1:
        raise HTTPException(status_code=404, detail=f"User with username '{username}' not found")

    return user

from fastapi import APIRouter, HTTPException

from api.jobs.job import Job
from mongo.mongo import job_collection

URL_JOBS = "/jobs"
URL_JOBS_JOB_ID = URL_JOBS + "/{job_id}"

router = APIRouter()


@router.get(URL_JOBS)
def get_jobs():
    job_list = job_collection.find({}, {'_id': False})
    return list(job_list)


@router.get(URL_JOBS_JOB_ID)
def get_job(job_id: str):
    return job_collection.find_one({"id": job_id}, {'_id': False})


@router.post(URL_JOBS)
def insert_job(job: Job):
    job_collection.insert_one(job.dict())
    return job


@router.put(URL_JOBS_JOB_ID)
def update_job(job_id: str, job: Job):
    result = job_collection.update_one({"id": job_id}, {"$set": job.dict()})

    if result.matched_count != 1:
        raise HTTPException(status_code=404, detail=f"Job with id '{job_id}' not found")

    return job


@router.delete(URL_JOBS_JOB_ID)
def delete_job(job_id: str):
    job_collection.delete_one({"id": job_id})

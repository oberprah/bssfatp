from pydantic import BaseModel


class Application(BaseModel):
    id: str
    jobId: str
    username: str

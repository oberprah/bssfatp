import datetime

from pydantic import BaseModel


class Message(BaseModel):
    id: str
    applicationId: str
    username: str
    # In requests and responses will be represented as a str in ISO 8601 format, like: 2008-09-15T15:53:00+05:00
    date: datetime.datetime
    message: str

from fastapi import APIRouter

from api.jobs.applications.messages.message import Message
from mongo.mongo import message_collection

URL_MESSAGES = "/messages"

router = APIRouter()


@router.get(URL_MESSAGES)
def get_messages(applicationId: str = None):
    query = dict()
    if applicationId:
        query.update({'applicationId': applicationId})
    message_list = message_collection.find(query, {'_id': False})
    return list(message_list)


@router.get(URL_MESSAGES + "/{message_id}")
def get_message(message_id: str):
    return message_collection.find_one({"id": message_id}, {'_id': False})


@router.post(URL_MESSAGES)
def insert_message(message: Message):
    message_collection.insert_one(message.dict())
    return message

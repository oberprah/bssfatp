from fastapi import APIRouter

from api.jobs.applications.application import Application
from mongo.mongo import application_collection

URL_APPLICATIONS = "/applications"
URL_APPLICATIONS_APPLICATION_ID = URL_APPLICATIONS + "/{application_id}"

router = APIRouter()


@router.get(URL_APPLICATIONS)
def get_applications(jobId: str = None, username: str = None):
    query = dict()
    if jobId:
        query.update({'jobId': jobId})
    if username:
        query.update({'username': username})
    application_list = application_collection.find(query, {'_id': False})
    return list(application_list)


@router.get(URL_APPLICATIONS_APPLICATION_ID)
def get_application(application_id: str):
    return application_collection.find_one({"id": application_id}, {'_id': False})


@router.post(URL_APPLICATIONS)
def insert_application(application: Application):
    application_collection.insert_one(application.dict())
    return application

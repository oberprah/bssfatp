from typing import List

from pydantic import BaseModel


class Job(BaseModel):
    id: str
    username: str
    title: str
    description: str = None
    tags: List[str]
    image: str
    jobTypes: List[str]
    expectedEarnings: int = None
    location: str

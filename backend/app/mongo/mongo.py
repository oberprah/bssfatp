from pymongo import MongoClient

client = MongoClient("mongo", 27017)

db = client['HireYourInfluencer']
user_collection = db['users']
job_collection = db['jobs']
application_collection = db['application']
message_collection = db['messages']

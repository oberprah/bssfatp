# Setup

Build and tag the Docker image:

```zsh
$ docker build -t <name>:dev .
```

Then, spin up the container once the build is done:

```zsh
$ docker run -v ${PWD}:/app -p 4000:80 --rm <name>:dev
```

Try it out: [http://localhost:4000/docs](http://localhost:4000/docs)

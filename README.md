# Setup

Build the images and fire up the containers. The `-d` runs the containers in the background.

```zsh
$ docker-compose up --build -d
```

Try it out. The webpage can be found at [localhost:3000](http://localhost:3000/) and the documentation of the backend API at [localhost:4000/docs](http://localhost:4000/docs).

The mongoDB runs at port `27017`.

Bring down the container:

```zsh
$ docker-compose stop
```
